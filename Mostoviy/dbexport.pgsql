--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: books; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.books (
    book_id integer NOT NULL,
    name text,
    publisher text,
    price smallint
);


ALTER TABLE public.books OWNER TO postgres;

--
-- Name: books_book_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.books_book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.books_book_id_seq OWNER TO postgres;

--
-- Name: books_book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.books_book_id_seq OWNED BY public.books.book_id;


--
-- Name: drivers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.drivers (
    driver_id integer NOT NULL,
    name text,
    xp_months smallint,
    CONSTRAINT drivers_xp_months_check CHECK ((xp_months > 0))
);


ALTER TABLE public.drivers OWNER TO postgres;

--
-- Name: drivers_driver_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.drivers_driver_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.drivers_driver_id_seq OWNER TO postgres;

--
-- Name: drivers_driver_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.drivers_driver_id_seq OWNED BY public.drivers.driver_id;


--
-- Name: last_stops; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.last_stops (
    stop_id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.last_stops OWNER TO postgres;

--
-- Name: last_stops_stop_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.last_stops_stop_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.last_stops_stop_id_seq OWNER TO postgres;

--
-- Name: last_stops_stop_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.last_stops_stop_id_seq OWNED BY public.last_stops.stop_id;


--
-- Name: regular_stops; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regular_stops (
    stop_id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.regular_stops OWNER TO postgres;

--
-- Name: regular_stops_stop_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.regular_stops_stop_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.regular_stops_stop_id_seq OWNER TO postgres;

--
-- Name: regular_stops_stop_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.regular_stops_stop_id_seq OWNED BY public.regular_stops.stop_id;


--
-- Name: tracks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tracks (
    track_number integer NOT NULL,
    first_stop integer,
    last_stop integer
);


ALTER TABLE public.tracks OWNER TO postgres;

--
-- Name: trolleybus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trolleybus (
    driver_id integer,
    track_id integer,
    registration_number integer NOT NULL
);


ALTER TABLE public.trolleybus OWNER TO postgres;

--
-- Name: books book_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.books ALTER COLUMN book_id SET DEFAULT nextval('public.books_book_id_seq'::regclass);


--
-- Name: drivers driver_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.drivers ALTER COLUMN driver_id SET DEFAULT nextval('public.drivers_driver_id_seq'::regclass);


--
-- Name: last_stops stop_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.last_stops ALTER COLUMN stop_id SET DEFAULT nextval('public.last_stops_stop_id_seq'::regclass);


--
-- Name: regular_stops stop_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regular_stops ALTER COLUMN stop_id SET DEFAULT nextval('public.regular_stops_stop_id_seq'::regclass);


--
-- Data for Name: books; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.books (book_id, name, publisher, price) FROM stdin;
2	IT	Книжковий клуб	\N
1	IT	USA publisher	100
3	Dark tower	USA publisher	130
4	Dark tower	EBUP	150
5	Pets graveyard	EPUB	190
\.


--
-- Data for Name: drivers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.drivers (driver_id, name, xp_months) FROM stdin;
1	Cool Driver	48
2	Medium Driver	24
3	Noob Driver	5
4	Junior Driver	5
5	Petya Pyatoschkin	12
\.


--
-- Data for Name: last_stops; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.last_stops (stop_id, name) FROM stdin;
1	Aurora
2	Azot
3	Airport
4	Sanatorium
5	River port
\.


--
-- Data for Name: regular_stops; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regular_stops (stop_id, name) FROM stdin;
1	Taras Shevchenko aley
2	Sales House
3	Square of 700 birthday of Cherkassy
4	Univercity
5	Univercity street
\.


--
-- Data for Name: tracks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tracks (track_number, first_stop, last_stop) FROM stdin;
10	1	2
1	1	4
2	3	5
7	2	4
8	3	2
\.


--
-- Data for Name: trolleybus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trolleybus (driver_id, track_id, registration_number) FROM stdin;
1	2	1023
2	1	2039
3	5	3892
5	3	8372
4	4	3278
\.


--
-- Name: books_book_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.books_book_id_seq', 5, true);


--
-- Name: drivers_driver_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.drivers_driver_id_seq', 5, true);


--
-- Name: last_stops_stop_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.last_stops_stop_id_seq', 5, true);


--
-- Name: regular_stops_stop_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.regular_stops_stop_id_seq', 5, true);


--
-- Name: books books_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_pkey PRIMARY KEY (book_id);


--
-- Name: drivers drivers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.drivers
    ADD CONSTRAINT drivers_pkey PRIMARY KEY (driver_id);


--
-- Name: last_stops last_stops_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.last_stops
    ADD CONSTRAINT last_stops_pkey PRIMARY KEY (stop_id);


--
-- Name: regular_stops regular_stops_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regular_stops
    ADD CONSTRAINT regular_stops_pkey PRIMARY KEY (stop_id);


--
-- Name: tracks tracks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tracks
    ADD CONSTRAINT tracks_pkey PRIMARY KEY (track_number);


--
-- Name: trolleybus trolleybus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trolleybus
    ADD CONSTRAINT trolleybus_pkey PRIMARY KEY (registration_number);


--
-- Name: trolleybus trolleybus_registration_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trolleybus
    ADD CONSTRAINT trolleybus_registration_number_key UNIQUE (registration_number);


--
-- Name: tracks tracks_first_stop_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tracks
    ADD CONSTRAINT tracks_first_stop_fkey FOREIGN KEY (first_stop) REFERENCES public.last_stops(stop_id);


--
-- Name: tracks tracks_last_stop_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tracks
    ADD CONSTRAINT tracks_last_stop_fkey FOREIGN KEY (last_stop) REFERENCES public.last_stops(stop_id);


--
-- Name: trolleybus trolleybus_driver_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trolleybus
    ADD CONSTRAINT trolleybus_driver_id_fkey FOREIGN KEY (driver_id) REFERENCES public.drivers(driver_id);


--
-- PostgreSQL database dump complete
--

