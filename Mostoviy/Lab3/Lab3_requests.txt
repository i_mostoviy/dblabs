1 Request with between and not between
SELECT * FROM books
	WHERE price BETWEEN 100 AND 170

SELECT * FROM books
	WHERE price NOT BETWEEN 100 AND 170

2 Request with IN and NOT IN
SELECT * FROM books
	WHERE price IN (100, 150, 130)

SELECT * FROM books
	WHERE price NOT IN (100, 150, 130)

3 Request with LIKE 
SELECT * FROM books
	WHERE name LIKE('D%')

SELECT * FROM books
	WHERE name NOT LIKE('D%')

SELECT * FROM books
	WHERE name LIKE('%r')

4 Request with IS NULL and IS NOT NULL
SELECT * FROM books
	WHERE price IS NULL

SELECT * FROM books
	WHERE price IS NOT NULL

5 Requests with calculations
SELECT name,(price - price*0.1) AS sale_price FROM books
	WHERE price IS NOT NULL

SELECT name,(price + price*0.1) AS price_in_shops FROM books
	WHERE price IS NOT NULL

SELECT name,(price * 0.1) AS profit FROM books
	WHERE price IS NOT NULL